###############################################################################
# Aqua Journey                                                                #
# Copyright (C) 2018 Michael Alexsander Silva Dias                            #
#-----------------------------------------------------------------------------#
# This file is part of Aqua Journey.                                          #
#                                                                             #
# Aqua Journey is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Affero General Public License as published    #
# by the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                         #
#                                                                             #
# Aqua Journey is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Affero General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with Aqua Journey.  If not, see <http://www.gnu.org/licenses/>.       #
###############################################################################

extends MarginContainer

onready var _main = $Background/Main
onready var _about = $Background/About

onready var tree = get_tree()

func _ready():
	pass

func _on_play_pressed():
	tree.paused = false
	
	visible = false

func _on_about_pressed():
	_main.visible = false
	_about.visible = true

func _on_exit_pressed():
	tree.quit()

func _on_back_pressed():
	_main.visible = true
	_about.visible = false
