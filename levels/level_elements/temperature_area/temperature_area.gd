###############################################################################
# Aqua Journey                                                                #
# Copyright (C) 2018 Michael Alexsander Silva Dias                            #
#-----------------------------------------------------------------------------#
# This file is part of Aqua Journey.                                          #
#                                                                             #
# Aqua Journey is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Affero General Public License as published    #
# by the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                         #
#                                                                             #
# Aqua Journey is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Affero General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with Aqua Journey.  If not, see <http://www.gnu.org/licenses/>.       #
###############################################################################

tool
extends Polygon2D

enum TemperatureLevels {
	VERY_LOW,
	LOW,
	HIGH,
	VERY_HIGH
}

export(TemperatureLevels) var temperature_level = 0

var _temperature = -100.0

func _ready():
	if not Engine.editor_hint:
		set_process(false)
	
	_update_temperature()
	
	$Area/Collision.polygon = polygon

# Editor only.
func _process(delta):
	_update_temperature()

func _update_temperature():
	match temperature_level:
		TemperatureLevels.VERY_LOW:
			_temperature = -100.0
			
			color = ColorN("blue")
		TemperatureLevels.LOW:
			_temperature = -25.0
			
			color = ColorN("teal")
		TemperatureLevels.HIGH:
			_temperature = 125.0
			
			color = ColorN("orange")
		TemperatureLevels.VERY_HIGH:
			_temperature = 200.0
			
			color = ColorN("red")

func get_temperature():
	return _temperature
