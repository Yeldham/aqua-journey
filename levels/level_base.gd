###############################################################################
# Aqua Journey                                                                #
# Copyright (C) 2018 Michael Alexsander Silva Dias                            #
#-----------------------------------------------------------------------------#
# This file is part of Aqua Journey.                                          #
#                                                                             #
# Aqua Journey is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Affero General Public License as published    #
# by the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                         #
#                                                                             #
# Aqua Journey is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Affero General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with Aqua Journey.  If not, see <http://www.gnu.org/licenses/>.       #
###############################################################################

extends Node

export(PackedScene) var _next_level

onready var _tilemap = $TileMap

onready var _player = $Player

onready var _tween = $Tween

func _on_player_state_changed(state):
	match state:
		0:
			AudioServer.set_bus_effect_enabled(1, 0, false)
			AudioServer.set_bus_effect_enabled(1, 1, false)
		1:
			AudioServer.set_bus_effect_enabled(1, 0, true)
			AudioServer.set_bus_effect_enabled(1, 1, false)
		2:
			AudioServer.set_bus_effect_enabled(1, 0, false)
			AudioServer.set_bus_effect_enabled(1, 1, true)

func _on_player_ambient_temperature_changed(temperature, on_ready=true):
	var color
	if temperature > 0 and temperature < 100:
		color = ColorN("white")
	elif temperature <= 0:
		if temperature <= -50:
			color = ColorN("royalblue")
		else:
			color = ColorN("cyan")
	elif temperature >= 100:
		if temperature >= 150:
			color = ColorN("tomato")
		else:
			color = ColorN("goldenrod")
	
	if on_ready:
		_tween.interpolate_property(_tilemap, "modulate", _tilemap.modulate,
				color, 0.4, Tween.TRANS_LINEAR, Tween.EASE_IN)
		_tween.start()
	else:
		$TileMap.modulate = color  # 'onready' won't be fast enough.

func _on_player_level_finished():
	GameManager.switch_scene(_next_level)
