# Aqua Journey

A game where you control a drop of water, changing its states to get to the goal.

This game was made for the [GodotJam - June 2018](https://itch.io/jam/godotjam062018).