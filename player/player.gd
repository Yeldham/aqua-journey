###############################################################################
# Aqua Journey                                                                #
# Copyright (C) 2018 Michael Alexsander Silva Dias                            #
#-----------------------------------------------------------------------------#
# This file is part of Aqua Journey.                                          #
#                                                                             #
# Aqua Journey is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Affero General Public License as published    #
# by the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                         #
#                                                                             #
# Aqua Journey is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Affero General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with Aqua Journey.  If not, see <http://www.gnu.org/licenses/>.       #
###############################################################################

extends KinematicBody2D

signal state_changed
signal ambient_temperature_changed
signal level_finished

enum States {
	LIQUID,
	SOLID,
	GAS
}

const AudioSamples = {
	COLLISION_LIQUID = preload("res://player/assets/collision_liquid.wav"),
	COLLISION_SOLID = preload("res://player/assets/collision_solid.wav"),
	STATE_SOLID = preload("res://player/assets/state_solid.wav"),
	STATE_GAS = preload("res://player/assets/state_gas.wav"),
	DIE_SOLID = preload("res://player/assets/die_solid.wav")
}

const TEMPERATURE_CHANGE_STEP = 0.02

const SPEED_GO_MAX = 250
const SPEED_GO_STEP = 0.15
const SPEED_GRAVITY_MAX = 2500
const GRAVITY = 2800

const JUMP_FORCE = -900

const CAMERA_MARGIN_TOP = 0.51

const EYES_DISTANCE_MAX = 16

onready var _particles = $Particles

onready var _body = $Body
onready var _eyes = $Body/Eyes

onready var _area = $Area

onready var _camera = $Camera

onready var _collision_audio = $CollisionAudio
onready var _state_audio = $StateAudio

onready var _animation = $Animation
onready var _tween = $Tween

onready var _hud_temperature = $Canvas/HUD/Temperature

export(States) var state = States.LIQUID setget _set_state

var _temperature = 50.0
var _ambient_temperature = 50.0

var _speed_current = Vector2()

var _is_jumping = false

var _old_direction = Vector2()

func _ready():
	match state:
		States.LIQUID:
			_collision_audio.play()
		States.SOLID:
			_temperature = -25
			_ambient_temperature = _temperature
			
			_set_state(States.SOLID)
		States.GAS:
			_temperature = 125
			_ambient_temperature = _temperature
			
			_set_state(States.GAS)
	
	emit_signal("ambient_temperature_changed", _ambient_temperature, false)

func _physics_process(delta):
	var direction = Vector2()
	if Input.is_action_pressed("player_go_left"):
		direction.x -= 1
	if Input.is_action_pressed("player_go_right"):
		direction.x += 1
	
	### State Behaviour ###
	
	match state:
		States.LIQUID:
			if _speed_current.y < SPEED_GRAVITY_MAX:
				_speed_current.y += GRAVITY * delta
			
			var is_on_floor = is_on_floor()
			if is_on_floor:
				
				if Input.is_action_just_pressed("player_go_up"):
					_speed_current.y = JUMP_FORCE
					
					_is_jumping = true
					
					_eyes.frame = 0
					
					_collision_audio.play()
					
					_animation.play("Jump (liquid)")
				elif direction.x != 0 and not is_on_wall():
					if _animation.assigned_animation != "Move (liquid)":
						_body.frame = 1
						_eyes.frame = 0
						
						_animation.play("Move (liquid)")
				elif _animation.assigned_animation != "Idle (liquid)":
					_body.frame = 1
					_body.position.y = 0
					_body.scale = Vector2(1, 1)
					
					_animation.play("Idle (liquid)")
			elif _is_jumping:
				if Input.is_action_just_released("player_go_up"):
					_speed_current.y /= 2
					
					_is_jumping = false
				elif _speed_current.y > 0:
					_is_jumping = false
			elif _animation.assigned_animation != "Jump (liquid)":
				_eyes.frame = 0
				_animation.play("Jump (liquid)")
			
			_speed_current.x = lerp(_speed_current.x,
					SPEED_GO_MAX * direction.x, SPEED_GO_STEP)
			
			# Center the camera back when landing.
			if is_on_floor and position.y < _camera.get_camera_position().y:
				_camera.drag_margin_top = 0
			elif _camera.drag_margin_top == 0:
				_camera.drag_margin_top = CAMERA_MARGIN_TOP
			
			if _old_direction != direction:
				_tween.interpolate_property(_eyes, "position:x",
						_eyes.position.x, EYES_DISTANCE_MAX * direction.x, 0.5,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				_tween.start()
		
		States.SOLID:
			if Input.is_action_pressed("player_go_up"):
				direction.y -= 1
			
			_speed_current.x = lerp(_speed_current.x,
					SPEED_GO_MAX * direction.x, SPEED_GO_STEP)
			
			var is_on_wall = is_on_wall()
			var is_on_ceiling = is_on_ceiling()
			if is_on_wall or is_on_ceiling:
				_speed_current.y = lerp(_speed_current.y,
						SPEED_GO_MAX * direction.y, SPEED_GO_STEP)
			
			var is_on_floor = is_on_floor()
			if not (is_on_wall and (direction.y < 0 or not is_on_floor)) and\
					not is_on_ceiling:
				_speed_current.y += GRAVITY * delta
			
			if direction.x != 0 and not _collision_audio.playing and\
					(is_on_floor or is_on_wall or is_on_ceiling) and\
					not (is_on_wall and (is_on_ceiling or is_on_floor)) and\
					not (is_on_wall and direction.y >= 0):
				_collision_audio.play()
				
				var rotation_direction = 90*direction.x
				if is_on_ceiling:
					rotation_direction *= -1
				_tween.interpolate_property(_body, "rotation_degrees",
						_body.rotation_degrees,
						_body.rotation_degrees + rotation_direction, 0.2,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
				_tween.start()
		
		States.GAS:
			if Input.is_action_pressed("player_go_up"):
				direction.y -= 1
			if Input.is_action_pressed("player_go_down"):
				direction.y += 1
			
			_speed_current.x = lerp(_speed_current.x,
					SPEED_GO_MAX * direction.x, SPEED_GO_STEP)
			_speed_current.y = lerp(_speed_current.y,
					SPEED_GO_MAX * direction.y, SPEED_GO_STEP)
			
			if _old_direction != direction:
				_tween.interpolate_property(_eyes, "position", _eyes.position,
						Vector2(EYES_DISTANCE_MAX, EYES_DISTANCE_MAX) *\
						direction, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
				_tween.start()
	
	_speed_current = move_and_slide(_speed_current, Vector2(0, -1))
	
	_old_direction = direction
	
	
	### Temperature Update ###
	
	_temperature = lerp(_temperature, _ambient_temperature,
			TEMPERATURE_CHANGE_STEP)
	
	var new_state
	if _temperature <= 0:
		if _temperature <= -75:
			die()
			
			return
		
		new_state = States.SOLID
	elif _temperature >= 100:
		if _temperature >= 175:
			die()
			
			return
		
		new_state = States.GAS
	elif _temperature >= 25 and _temperature <= 75:
		new_state = States.LIQUID
	
	_hud_temperature.value = _temperature
	
	if new_state != null and new_state != state:
		_set_state(new_state)

func _set_state(new_state):
	if not is_inside_tree():
		state = new_state
		
		return
	
	# Reset values depending on the last state.
	match state:
		States.LIQUID:
			_body.position = Vector2()
			_body.scale = Vector2(1, 1)
			
			_eyes.position.y = 0
			
			_camera.drag_margin_top = 0
		States.SOLID:
			_old_direction.x = 0
			
			_tween.stop(_body, "rotation_degrees")
			
			_body.rotation = 0
		States.GAS:
			_particles.emitting = false
			_particles.visible = false
			
			_body.self_modulate.a = 1
			
			_eyes.position.y = 0
			_eyes.rotation = 0
	
	state = new_state
	
	# Set the new values.
	match state:
		States.LIQUID:
			if is_on_floor():
				_body.frame = 1
			else:
				_body.frame = 0
			
			_eyes.frame = 0
			
			_collision_audio.stream = AudioSamples.COLLISION_LIQUID
			_state_audio.stream = AudioSamples.COLLISION_LIQUID
			_state_audio.play()
			
			_animation.play("Idle (liquid)")
		States.SOLID:
			_body.frame = 2
			
			_eyes.frame = 1
			
			_camera.drag_margin_top = CAMERA_MARGIN_TOP
			
			_collision_audio.stream = AudioSamples.COLLISION_SOLID
			_state_audio.stream = AudioSamples.STATE_SOLID
			_state_audio.play()
			
			_animation.play("Idle (solid)")
			
			_tween.interpolate_property(_eyes, "position",
						_eyes.position, Vector2(), 0.5,
						Tween.TRANS_CUBIC, Tween.EASE_OUT)
			_tween.start()
		States.GAS:
			_particles.emitting = true
			_particles.visible = true
			
			_body.self_modulate.a = 0
			
			_eyes.frame = 2
			
			_collision_audio.stream = AudioSamples.STATE_GAS
			_state_audio.stream = AudioSamples.STATE_GAS
			_state_audio.play()
			
			_animation.play("Idle (gas)")
	
	emit_signal("state_changed", new_state)

func die():
	set_physics_process(false)
	
	_eyes.frame = 3
	
	match state:
		States.SOLID:
			_body.frame = 3
			
			_state_audio.stream = AudioSamples.DIE_SOLID
			_state_audio.play()
			
			_animation.stop()
		States.GAS:
			_animation.play("Die (gas)")
	
	GameManager.reload_scene()

func _on_area_entered(body):
	if body.is_in_group("Temperature Areas"):
		var temperature = body.get_parent().get_temperature()
		if temperature != _ambient_temperature:
			_ambient_temperature = temperature
			
			emit_signal("ambient_temperature_changed", temperature)
	elif body.is_in_group("Level Exit"):
		set_physics_process(false)
		
		emit_signal("level_finished")

func _on_area_exited(body):
	var overlapping_areas = _area.get_overlapping_areas()
	overlapping_areas.erase(body)
	var new_ambient_temperature
	if not overlapping_areas.empty():
		new_ambient_temperature =\
				overlapping_areas.back().get_parent().get_temperature()
	else:
		new_ambient_temperature = 50.0
	
	if new_ambient_temperature != _ambient_temperature:
		_ambient_temperature = new_ambient_temperature
		
		emit_signal("ambient_temperature_changed", new_ambient_temperature)
