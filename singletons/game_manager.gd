###############################################################################
# Aqua Journey                                                                #
# Copyright (C) 2018 Michael Alexsander Silva Dias                            #
#-----------------------------------------------------------------------------#
# This file is part of Aqua Journey.                                          #
#                                                                             #
# Aqua Journey is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Affero General Public License as published    #
# by the Free Software Foundation, either version 3 of the License, or        #
# (at your option) any later version.                                         #
#                                                                             #
# Aqua Journey is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Affero General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Affero General Public License    #
# along with Aqua Journey.  If not, see <http://www.gnu.org/licenses/>.       #
###############################################################################

extends Node

onready var _animation = $Animation

onready var _menu = $CanvasLayer/Menu

onready var _tree = get_tree()

var _new_scene

func _ready():
	_tree.paused = true
	
	$Audio.play()

func _input(event):
	if _animation.current_animation == "Fade In":
		return
	
	if event.is_action_pressed("pause"):
		_tree.paused = not _tree.paused
		
		_menu.visible = not _menu.visible

func reload_scene():
	_animation.play("Fade In")

func switch_scene(scene):
	_new_scene = scene
	
	_animation.play("Fade In")

func _on_animation_finished(anim_name):
	if anim_name != "Fade In":
		return
	
	if _new_scene != null:
		get_tree().change_scene_to(_new_scene)
		
		_new_scene = null
	else:
		get_tree().reload_current_scene()
	
	_animation.play("Fade Out")
